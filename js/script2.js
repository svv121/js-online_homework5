//"use strict";
/*
1. Метод - це властивість - функція внутри об'єкту для здійснення якіх дій.
2. Значення властивостей можуть мати будь-який тип, включаючи інші об'єкти.
3. У змінній зберігається ні саме об'єкт, а посилання на ділянку фізичної пам'яті, у який зберігається об'єкт, його властивості та їхні значення.
*/
let firstName;
let lastName;
do {
    firstName = prompt("Please, enter your first name", "")
    }
while (!firstName || firstName.trim().length === 0);
do {
    lastName = prompt("Please, enter your last name", "");
}
while (!lastName || lastName.trim().length === 0);
const createNewUser = () => {
return {
    _firstName: firstName,
    _lastName: lastName,
    get firstName() {
        return this._firstName;
    },
    get lastName() {
        return this._lastName;
    },
    setFirstName(newValue) {
        if (newValue.trim().length > 0) {
            this._firstName = newValue}
        return this._firstName
           },
    setLastName(newValue) {
        if (newValue.trim().length > 0) {
            this._lastName = newValue}
        return this._lastName
    },
    getLogin(){
        return (this.firstName[0] + this.lastName).toLowerCase();
    },
}
}
const newUser = createNewUser();
console.log(newUser);
console.log("------------");
console.log(newUser.getLogin());
console.log("------------");
console.log(`${newUser.firstName} ${newUser.lastName}`);
console.log("------------");
newUser.firstName = 'Pete';
newUser.lastName = 'Stevenson';
console.log(`${newUser.firstName} ${newUser.lastName}`);
console.log("------------");
newUser.setFirstName("William");
newUser.setLastName("Stave");
console.log(`${newUser.firstName} ${newUser.lastName}`);